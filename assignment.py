#!/usr/bin/env python3

# Robert Winkler (226 474)
# Control Engineering and Robotics - Embedded Robotics
#
#
#  Task:
# ----------------------------------------------------------------------------
# Jones, Jimenez, and Sihota (JJS) is expanding its tax service business into
# the San Antonio area. The company wishes to be able to service at least 100
# personal and 25 corporate accounts per week. JJS plans to hire three
# levels of employees: CPAs, experienced accountants without a CPA, and
# junior accountants. The following table gives the weekly salary level
# as well as the projection of the expected number of accounts that can be
# serviced weekly by each level of employee:
#
#  +--------------+-------------+-------------+----------+
#  |   Employee   |  per. acc.  |  corp. acc. |  Salary  |
#  +--------------+-------------+-------------+----------+
#  | CPA          |           6 |           3 |    $1400 |
#  | Experienced  |           6 |           1 |     $900 |
#  | Junior       |           4 |           0 |     $600 |
#  +--------------+-------------+-------------+----------+
#
# JJS wishes to staff its San Antonio office so that at least two-thirds of
# all its employees will be either experienced or junior accountants.
# Determine the number of employees from each experience level the firm should
# hire for its San Antonio office to minimize its total weekly payroll.
# Do it using an integer programming model.

from pulp import *

def main():

    # Create LP Problem
    prob = LpProblem("JJS_tax_service", LpMinimize)

    # Create Variables
    xc = LpVariable("Number_of_CPA_employees", lowBound=0, upBound=None, cat='Integer')
    xe = LpVariable("Number_of_experienced_employees", lowBound=0, upBound=None, cat='Integer')
    xj = LpVariable("Number_of_junior_employees", lowBound=0, upBound=None, cat='Integer')

    # Add the objective function
    prob += 1400*xc + 900*xe + 600*xj, "Cost of employment"

    # Add constrains
    prob += 6*xc + 6*xe + 4*xj >= 100, "Minimum of serviced personal accounts"
    prob += 3*xc + xe >= 25, "Minimum of serviced company accounts"
    prob += xe + xj >= 2*xc, "At least 2/3 experienced and junior employees"

    # Solve the problem
    prob.solve()

    # Print the results
    print("--------------------- Task summary ---------------------")
    print(" 1. Solution status:", LpStatus[prob.status])
    print(" 2. Optimal variables values:")
    for v in prob.variables():
        print("    ", v.name, "=", v.varValue)
    print(" 3. Total Cost of employment = ", value(prob.objective))
    print("--------------------------------------------------------")

if __name__ == "__main__":
    main()
