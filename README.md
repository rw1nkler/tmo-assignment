# Theory and Methods of Optimization - Assignment

In order to run the assignment program you should first install
the dependencies:

```
pip3 install -r requirements.txt
```

Then you are able to run the assignment program:
```
python3 assignment.py
```
